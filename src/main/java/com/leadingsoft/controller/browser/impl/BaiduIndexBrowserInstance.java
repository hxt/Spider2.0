package com.leadingsoft.controller.browser.impl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.configuration.Configuration;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefRequestContext;
import org.cef.handler.CefRequestContextHandlerAdapter;
import org.cef.network.CefCookie;
import org.cef.network.CefCookieManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leadingsoft.controller.browser.BrowserInstance;
import com.leadingsoft.core.CommonConfig;
import com.leadingsoft.ui.BrowserFrame;

public class BaiduIndexBrowserInstance implements BrowserInstance {
	private static Logger logger = LoggerFactory.getLogger(BaiduIndexBrowserInstance.class);
	private CefClient nowClient;
	private Boolean nowOsrEnabled;
	private Boolean nowTransparentPaintingEnabled;
	private Configuration config;
	private String cookiePath;

	public BaiduIndexBrowserInstance(CefClient client, boolean osrEnabled, boolean transparentPaintingEnabled,
			String cookiePath) {
		this.nowClient = client;
		this.nowOsrEnabled = osrEnabled;
		this.nowTransparentPaintingEnabled = transparentPaintingEnabled;
		this.config = CommonConfig.getInstance();
		this.cookiePath = cookiePath;
	}
	@Override
	public void createBrowserAndCrawler(Long startIndex, Long stopIndex) {
		System.out.println(config);
		CefRequestContext requestContext = null;
		if (cookiePath != null) {
			logger.debug("设置cookie文件地址");
			CefCookieManager cookieManager = CefCookieManager.getGlobalManager();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
			CefCookie cefCookie = null;
			try {
				cefCookie = new CefCookie("BDUSS",
						"0xzTXBwdEhhcEc2UTVYM0hmazQxZ0duR0pxWEdyMmRJWU5NLWRNSGlYTXUxMWxiQVFBQUFBJCQAAAAAAAAAAAEAAAB~Cbg5Znd3YW4xMzE0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC5KMlsuSjJbb",
						".baidu.com", "/", false, true, null, null, true, dateFormat.parse("2026-09-1214:14:04"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cookieManager.setCookie("http://index.baidu.com/", cefCookie);
			// CefCookieManager cookieManager = CefCookieManager.createManager(cookiePath, false);
			requestContext = CefRequestContext.createContext(new CefRequestContextHandlerAdapter() {
				@Override
				public CefCookieManager getCookieManager() {
					return cookieManager;
				}
			});
		}
		CefBrowser browser = nowClient.createBrowser("http://index.baidu.com/", nowOsrEnabled,
				nowTransparentPaintingEnabled, requestContext);
		final BrowserFrame frame = new BrowserFrame(nowClient, browser);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				CefApp.getInstance().dispose();
			}
		});
		frame.setSize(1000, 500);
		frame.setVisible(true);
	}
}

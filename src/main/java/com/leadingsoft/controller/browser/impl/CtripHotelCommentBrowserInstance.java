package com.leadingsoft.controller.browser.impl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.ConnectionSource;
import org.beetl.sql.core.ConnectionSourceHelper;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefLoadHandlerAdapter;

import com.leadingsoft.common.CheckUrl;
import com.leadingsoft.common.Constant;
import com.leadingsoft.common.TaskQuene;
import com.leadingsoft.common.model.Hotel;
import com.leadingsoft.controller.browser.BrowserInstance;
import com.leadingsoft.controller.download.DownloadCtripComment;
import com.leadingsoft.controller.parse.ParsingCtripHotelComment;
import com.leadingsoft.core.CommonConfig;
import com.leadingsoft.ui.BrowserFrame;

/**
 * @ClassName UrlBrowserInstance_v2
 * @Description Url浏览器实例<br>
 *              进行数据抓取<br>
 *              下一页是通过修改浏览器地址进行获取<br>
 *              示例网站：携程酒店评论信息
 * @author gongym
 * @date 2018年6月7日 下午5:09:30
 */
public class CtripHotelCommentBrowserInstance implements BrowserInstance {
	private CefClient nowClient;
	private Boolean nowOsrEnabled;
	private Boolean nowTransparentPaintingEnabled;
	// 配置对象
	private Configuration config;

	public CtripHotelCommentBrowserInstance(CefClient client, boolean osrEnabled, boolean transparentPaintingEnabled) {
		this.nowClient = client;
		this.nowOsrEnabled = osrEnabled;
		this.nowTransparentPaintingEnabled = transparentPaintingEnabled;
		this.config = CommonConfig.getInstance();
	}
	@Override
	public void createBrowserAndCrawler(Long start, Long size) {
		// 给一个起始页创建浏览器对象
		CefBrowser browser = nowClient.createBrowser(Constant.DEFAULTURL, nowOsrEnabled, nowTransparentPaintingEnabled);
		// 数据库连接对象
		String datasourceDriver = config.getString("datasource.driver");
		String datasourceUrl = config.getString("datasource.url");
		String datasourceUsername = config.getString("datasource.username");
		String datasourcePassword = config.getString("datasource.password");
		ConnectionSource source = ConnectionSourceHelper.getSimple(datasourceDriver, datasourceUrl, datasourceUsername,
				datasourcePassword);
		DBStyle mysql = new MySqlStyle();
		SQLLoader loader = new ClasspathLoader("/");
		UnderlinedNameConversion nc = new UnderlinedNameConversion();
		SQLManager sqlManager = new SQLManager(mysql, loader, source, nc, new Interceptor[] { new DebugInterceptor() });
		// 获取配置文件对象，判断是下载页面还是保存数据库
		Integer isDownload = config.getInt("is_download");
		List<Hotel> hotelList = sqlManager.all(Hotel.class, start, size);
		List<String> hotelIdList = new ArrayList<String>();
		hotelList.forEach((hotel) -> {
			hotelIdList.add(hotel.getHotelId());
		});
		nowClient.addLoadHandler(new CefLoadHandlerAdapter() {
			@Override
			public void onLoadEnd(CefBrowser browser, CefFrame frame, int what) {
				if (frame.isFocused() && frame.isMain()) {
					String url = browser.getURL();
					if (CheckUrl.isCtripUrl(url)) {
						if (isDownload.equals(1)) {
							// 开始下载文件
							DownloadCtripComment downloadCtripComment = new DownloadCtripComment(browser, hotelList,
									hotelIdList);
							browser.getSource(downloadCtripComment);
						} else {
							String listSelector = "comment_block";
							ParsingCtripHotelComment parsingCtripHotelComment = new ParsingCtripHotelComment(browser,
									listSelector, sqlManager, hotelList, hotelIdList);
							browser.getSource(parsingCtripHotelComment);
						}
					} else {
						String startUrl = TaskQuene.getCtripHotelCommentListUrl(url, hotelList, hotelIdList);
						TaskQuene.ctripCommentTaskUrl.add(startUrl);
					}
				}
			}
		});
		final BrowserFrame frame = new BrowserFrame(nowClient, browser);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				CefApp.getInstance().dispose();
			}
		});
		frame.setSize(1000, 500);
		frame.setVisible(true);
		// 开始抓取数据
		crawlerData(browser);
	}
	/**
	 * @Title: crawlerData
	 * @Description: 循环列表抓取数据
	 * @param browser
	 * @return: void
	 */
	private void crawlerData(CefBrowser browser) {
		while (true) {// 循环调用，如果队列中有任务就会加载页面
			String ctripHotelListUrl = TaskQuene.ctripCommentTaskUrl.poll();
			if (null != ctripHotelListUrl) {
				// 加载页面
				browser.loadURL(ctripHotelListUrl);
				try {
					Thread.sleep(Constant.SLEEP);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

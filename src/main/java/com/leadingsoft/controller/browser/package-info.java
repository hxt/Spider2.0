/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller.browser 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月10日 下午4:26:01 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 浏览器实例
 * @author gongym
 * @date 2018年6月10日 下午4:26:01
 */
package com.leadingsoft.controller.browser;

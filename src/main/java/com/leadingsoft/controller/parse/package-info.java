/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller.parse 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月7日 下午4:47:54 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 解析当前Html文档
 * @author gongym
 * @date 2018年6月7日 下午4:47:54
 */
package com.leadingsoft.controller.parse;

package com.leadingsoft.controller.parse.impl;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.leadingsoft.common.model.HotelComment;
import com.leadingsoft.controller.parse.GetDocumentData;

/**
 * @ClassName GetCtripHotelComments
 * @Description 例子2、抓取酒店评论列表
 * @author gongym
 * @date 2018年6月11日 上午10:14:00
 */
public class GetCtripHotelComment implements GetDocumentData {
	@Override
	public Object elementToObject(Element element) {
		return null;
	}
	@Override
	public List<Object> elementsToObjects(Elements elements) {
		List<Object> hotelCommentList = new ArrayList<Object>();
		elements.forEach((document) -> {
			HotelComment hotelComment = new HotelComment();
			Elements commentContentElement = document.select("div.J_commentDetail");
			String commentContent = commentContentElement.get(0).text();
			hotelComment.setCommentContent(commentContent);
			hotelCommentList.add(hotelComment);
		});
		return hotelCommentList;
	}
}

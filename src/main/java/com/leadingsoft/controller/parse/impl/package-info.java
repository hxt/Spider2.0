/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.controller.parse.impl 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月8日 上午10:34:46 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 解析页面的实现类
 * @author gongym
 * @date 2018年6月8日 上午10:34:46
 */
package com.leadingsoft.controller.parse.impl;

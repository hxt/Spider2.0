/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title: package-info.java 
 * @Prject: Spider2.0
 * @Package: com.leadingsoft.controller.other 
 * @Description: 新版爬虫，使用Java调用浏览器引擎
 * @author: gongym   
 * @date: 2018年6月20日 下午11:33:23 
 * @version: V1.0   
 */
/**
 * @ClassName: package-info
 * @Description: 不使用浏览器引擎，使用Jsoup发送请求
 * @author: gongym
 * @date: 2018年6月20日 下午11:33:23
 */
package com.leadingsoft.controller.other;

package com.leadingsoft.common;

public class CheckUrl {
	public static boolean isCtripUrl(String url) {
		return url.contains("hotels.ctrip.com");
	}
	public static boolean isMusic163Url(String url) {
		return url.contains("music.163.com");
	}
	public static boolean isMmjpgUrl(String url) {
		return url.contains("www.mmjpg.com");
	}
}

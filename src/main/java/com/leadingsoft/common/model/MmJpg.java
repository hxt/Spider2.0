package com.leadingsoft.common.model;

import org.beetl.sql.core.annotatoin.AssignID;

public class MmJpg {
	private String mmId;
	private String mmUrl;
	private Integer mmPage;
	private String mmName;

	@AssignID
	public String getMmId() {
		return mmId;
	}
	public void setMmId(String mmId) {
		this.mmId = mmId == null ? null : mmId.trim();
	}
	public String getMmUrl() {
		return mmUrl;
	}
	public void setMmUrl(String mmUrl) {
		this.mmUrl = mmUrl == null ? null : mmUrl.trim();
	}
	public Integer getMmPage() {
		return mmPage;
	}
	public void setMmPage(Integer mmPage) {
		this.mmPage = mmPage;
	}
	public String getMmName() {
		return mmName;
	}
	public void setMmName(String mmName) {
		this.mmName = mmName;
	}
}

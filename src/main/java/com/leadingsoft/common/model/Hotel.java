package com.leadingsoft.common.model;

import org.beetl.sql.core.annotatoin.AssignID;

public class Hotel {
	private String hotelId;
	private String hotelName;
	private Integer hotelStrategymedal;
	private String hotelAddress;
	private Integer hotelPrice;
	private String hotelSpecialLabel;
	private String hotelFacility;
	private String hotelLevel;
	private Float hotelValue;
	private Integer hotelTotalJudgementScore;
	private Integer hotelJudgement;
	private String hotelRecommend;
	private Long pageIndex;

	@AssignID
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId == null ? null : hotelId.trim();
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName == null ? null : hotelName.trim();
	}
	public Integer getHotelStrategymedal() {
		return hotelStrategymedal;
	}
	public void setHotelStrategymedal(Integer hotelStrategymedal) {
		this.hotelStrategymedal = hotelStrategymedal;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress == null ? null : hotelAddress.trim();
	}
	public Integer getHotelPrice() {
		return hotelPrice;
	}
	public void setHotelPrice(Integer hotelPrice) {
		this.hotelPrice = hotelPrice;
	}
	public String getHotelSpecialLabel() {
		return hotelSpecialLabel;
	}
	public void setHotelSpecialLabel(String hotelSpecialLabel) {
		this.hotelSpecialLabel = hotelSpecialLabel == null ? null : hotelSpecialLabel.trim();
	}
	public String getHotelFacility() {
		return hotelFacility;
	}
	public void setHotelFacility(String hotelFacility) {
		this.hotelFacility = hotelFacility == null ? null : hotelFacility.trim();
	}
	public String getHotelLevel() {
		return hotelLevel;
	}
	public void setHotelLevel(String hotelLevel) {
		this.hotelLevel = hotelLevel == null ? null : hotelLevel.trim();
	}
	public Float getHotelValue() {
		return hotelValue;
	}
	public void setHotelValue(Float hotelValue) {
		this.hotelValue = hotelValue;
	}
	public Integer getHotelTotalJudgementScore() {
		return hotelTotalJudgementScore;
	}
	public void setHotelTotalJudgementScore(Integer hotelTotalJudgementScore) {
		this.hotelTotalJudgementScore = hotelTotalJudgementScore;
	}
	public Integer getHotelJudgement() {
		return hotelJudgement;
	}
	public void setHotelJudgement(Integer hotelJudgement) {
		this.hotelJudgement = hotelJudgement;
	}
	public String getHotelRecommend() {
		return hotelRecommend;
	}
	public void setHotelRecommend(String hotelRecommend) {
		this.hotelRecommend = hotelRecommend == null ? null : hotelRecommend.trim();
	}
	public Long getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Long pageIndex) {
		this.pageIndex = pageIndex;
	}
	@Override
	public String toString() {
		return "Hotel [hotelId=" + hotelId + ", hotelName=" + hotelName + ", hotelStrategymedal=" + hotelStrategymedal
				+ ", hotelAddress=" + hotelAddress + ", hotelPrice=" + hotelPrice + ", hotelSpecialLabel="
				+ hotelSpecialLabel + ", hotelFacility=" + hotelFacility + ", hotelLevel=" + hotelLevel
				+ ", hotelValue=" + hotelValue + ", hotelTotalJudgementScore=" + hotelTotalJudgementScore
				+ ", hotelJudgement=" + hotelJudgement + ", hotelRecommend=" + hotelRecommend + ", pageIndex="
				+ pageIndex + "]";
	}
}
